const assert = require('assert');
const path = require('path');
const fs = require('fs');

const VideoThumbnailGallery = require('../index');

describe('VideoThumbnailGallery', function() {
  describe('#generateThumbnails()', function() {
    it('should create multiple thumbnail JPG files for the video', function(done) {
      this.timeout(0);

      const videoThumbnailGallery = new VideoThumbnailGallery(path.join(__dirname, 'bin', 'video.mp4'), {});
      videoThumbnailGallery.generateThumbnails(function(err, files) {
        if (!!err) {
          console.error(err);
          assert.fail('There was an error with the generating of thumbnails');
          done();
          return;
        }

        assert.notStrictEqual(files.length, 0, 'Thumbnails are not generated');
        done();
      });
    });
  });

  describe('#generateThumbnails()', function() {
    it('should combine all thumbnails into one large jpeg', function(done) {
      this.timeout(0);

      const thumbnailPath = path.join(__dirname, 'bin', 'thumbnails');
      fs.readdir(thumbnailPath, function(err, files) {
        files.sort(function(a, b) {
          const aNumberStartIndex = a.lastIndexOf('_');
          const aNumberEndIndex = a.indexOf('.', aNumberStartIndex);
          const aNumber = parseInt(a.substr(aNumberStartIndex + 1, aNumberEndIndex - aNumberStartIndex - 1));

          const bNumberStartIndex = b.lastIndexOf('_');
          const bNumberEndIndex = b.indexOf('.', bNumberStartIndex);
          const bNumber = parseInt(b.substr(bNumberStartIndex + 1, bNumberEndIndex - bNumberStartIndex - 1));

          return aNumber - bNumber;
        });

        files = files.map(filePath => path.join(thumbnailPath, filePath));

        const videoThumbnailGallery = new VideoThumbnailGallery(path.join(__dirname, 'bin', 'video.mp4'), {});
        videoThumbnailGallery.combineThumbnails(files, path.join(__dirname, 'bin', 'video.jpg'), function(err, output) {
          if (!!err) {
            console.error(err);
            assert.fail('There was an error with the combining of thumbnails');
            done();
            return;
          }

          assert.notStrictEqual(files.length, null, 'Thumbnails are not combined');
          done();
        });

      });
    });
  });
});
Video Thumbnail Gallery
===================
With this module you can generate thumbnail galleries of given videos.
It does this using ffmpeg to generate thumbnails and sharp to merge them together.
The module will automatically calculate thumbnail size, unless overridden using the options.

How to use
-------------
Initialize the class and call the generateGallery method:

`const vtg = new VideoThumbnailGallery(inputFilePath, options);`

`vtg.generateGallery(outputFilePath, callback);`

Options
-------------
The following options are available:

###### thumbnailInterval
The interval between thumbnails in seconds.

###### thumbnailsPerRow
How many thumbnails should be placed on the same row before starting a new row.

###### thumbnailWidth
The width of each thumbnail.

###### thumbnailHeight
The height of each thumbnail.